# 思沃数字平台


---
## 系统服务架构图
![](image/system-microservices-architecture.png)

---

## 角色介绍

![](https://s3.cn-north-1.amazonaws.com.cn/tws-upload/images/1542250082629-c82c6a94-3e40-4ecb-8286-f438db17647e.png)

- 学员： 报名参加线上/线下训练营的学生、及其他人员  
- 助教： 来辅导、督促学员完成任务卡
- 出题人： 通过题库来出各类题目
- 训练营设计师： 设计训练营，管理任务卡
- 管理员： 通过后台管理来给学员和助教分配训练营，以及给学员分配角色

---

## 模块介绍  

![](./image/模块介绍.png)

- 训练营-学员： 学生通过训练营前台来完成任务卡。  
- 成长日志： 学员可以使用成长日志模块来记录自己每天的成长经历。  
- 训练营-助教： 助教可以通过该模块来查看学员每天的学习情况并给予反馈。  
- 训练营设计： 训练营设计师可以使用该模块来组织训练营以及管理任务卡。  
- 题目： 出题人可以使用该模块来出各种类型的题目。  
- 用户权限管理： 管理员可以使用该模块来给用户分配角色、添加用户到训练营。  
- 组织管理： 管理组织，可以创建组织，并添加组织管理员。  

---

## 仓库介绍

### 前端介绍

![](./image/web-service-introduce.png)

**使用训练营（course-center）服务的web**  
- [tws-basic-quiz-web（basicQuiz-app）](https://gitee.com/tws-system/course-center/tree/master/web/basicQuiz-app): 简单客观题;项目目前在名为course-center的仓库中；  
- [tws-subjective-quiz-web（subjective-app）](https://gitee.com/tws-system/course-center/tree/master/web/subjective-app): 主观题；项目目前在名为course-center的仓库中；  
- [tws-logic-quiz-web](https://gitee.com/tws-system/tws-logic-quiz-web): 逻辑题；  
- [tws-online-language-quiz-web](https://gitee.com/tws-system/tws-online-language-quiz-web): 单语言在线编程题；  
- [tws-simple-coding-quiz-web](https://gitee.com/tws-system/tws-simple-coding-quiz-web): 简单编程题；  
- [tws-instructor-web](https://gitee.com/tws-system/tws-instructor-web): 训练营设计；  
- [tws-student-web](https://gitee.com/tws-system/tws-student-web): 训练营前端；  
- [tws-admin-web](https://gitee.com/tws-system/tws-admin-web): 训练营学生助教管理；  

**使用用户（user-center）服务的web。**  
- [tws-user-center-web](https://gitee.com/tws-system/tws-user-center/tree/master/home-web)：个人中心，项目在名为tws-user-center的仓库中；

**使用通知（tws-notification-center）服务的web**  
- [tws-notification-web](https://gitee.com/tws-system/tws-notification/tree/master/web)：通知模块，项目在名为tws-notification的仓库中；  

**使用认证（tws-anth-center）服务的web**  
- [tws-auth-center-web](https://gitee.com/tws-system/tws-user-center-web)：登录/注册模块；  

**使用成长日志（growth-note-app）服务的web**  
- [growth-note-app-web](https://gitee.com/tws-system/growth-note-app/tree/master/web)：成长日志，项目在名为growth-note-app的仓库中；    

**使用考试中心（tws-exam-center）服务的web**
- [tws-exam-center-web](https://gitee.com/tws-system/tws-exam-center-web):考试中心；  

**静态页面web**  
- [thoughtworks.school](https://gitee.com/tws-system/thoughtworks.school)：首页  
- [tws-online-camp](https://gitee.com/tws-system/tws-online-camp)：训练营宣传页面  

### 后端服务介绍

![](./image/backend-service-introduce.png)

- [course-center](https://gitee.com/tws-system/course-center)  
需要quiz-center、user-center、notification-center提供相应的接口服务；
提供给user-center、notification-center相应的接口服务；
用于训练营的服务


- [user-center](https://gitee.com/tws-system/tws-user-center)  
需要course-center、notification-center提供相应的接口服务；
提供给course-center、notification-center、practice-diary(growth-note-app)、quiz-center相应的接口服务；
用于用户信息相关的服务。

- [practice-diary(growth-note-app)](https://gitee.com/tws-system/growth-note-app)  
需要user-center、notification-center提供相应的接口服务；用于成长日志相关服务；

- [quiz-center](https://gitee.com/tws-system/quiz-center)  
需要user-center、notification-center提供相应接口服务；
提供给course-center相应的接口服务；
用于题库相关的服务


- [notification-center](https://gitee.com/tws-system/tws-notification)  
需要user-center提供相应的接口服务；
提供给course-center、quiz-center、practice-diary(growth-note-app)、user-center相应的接口服务；
用于通知相关的服务

- [tws-auth-center](https://gitee.com/tws-system/tws-auth-center)  
需要user-center、course-center提供相应接口，用于信息认证.

- [tws-organization-center](https://gitee.com/tws-system/tws-organization-center)  
组织服务

- [tws-visualization-center](https://gitee.com/tws-system/tws-visualization-center)  
数据可视化服务

---

## 启动
- [启动文档](https://gitee.com/tws-system/tws-setup-quickly/blob/master/README.md)

---

## 技术栈

- `React` 、 `Ant Design`
- `SpringBoot` 、`Spring JPA`、`Spring Cloud` 、`Spring Security`
- `Docker`、`Nginx`、`K8s`、`Jenkins`

---
## 开发手册
- [前端开发手册](operatingGuide/前端开发手册.md)
- [后端开发手册](./operatingGuide/后端开发手册.md)

---

## 用户使用指南  

- [训练营-学员端](./operatingGuide/训练营-学员端.md)  
- [训练营-助教端](./operatingGuide/训练营-助教端.md)  
- [成长日志](./operatingGuide/成长日志.md)  
- [题库创建题目](./operatingGuide/题库创建题目.md)  
- [训练营设计](./operatingGuide/训练营设计.md)  