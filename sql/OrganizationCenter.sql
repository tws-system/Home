# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.28)
# Database: OrganizationCenter
# Generation Time: 2019-12-15 06:50:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table flyway_schema_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flyway_schema_history`;

CREATE TABLE `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;

INSERT INTO `flyway_schema_history` (`installed_rank`, `version`, `description`, `type`, `script`, `checksum`, `installed_by`, `installed_on`, `execution_time`, `success`)
VALUES
	(1,'1','create organization table','SQL','V1__create_organization_table.sql',-1073065782,'twschool','2019-02-23 15:38:45',16,1),
	(2,'2','create user orgranization table','SQL','V2__create_user_orgranization_table.sql',-1091031553,'twschool','2019-02-23 15:38:45',17,1),
	(3,'3','init orgranization data','SQL','V3__init_orgranization_data.sql',-516093154,'twschool','2019-02-23 15:38:45',6,1),
	(4,'4','migrate user to default organzation','SQL','V4__migrate_user_to_default_organzation.sql',1795402156,'twschool','2019-02-23 15:38:45',60,1),
	(5,'5','alter organizationId and userId with unique','SQL','V5__alter_organizationId_and_userId_with_unique.sql',1736363064,'twschool','2019-02-23 15:38:45',39,1);

/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table organization
# ------------------------------------------------------------

DROP TABLE IF EXISTS `organization`;

CREATE TABLE `organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;

INSERT INTO `organization` (`id`, `creator`, `title`, `createTime`)
VALUES
	(1,1,'思沃训练营','2019-02-23 15:38:45'),
	(2,11,'XIN','2019-02-23 16:27:27'),
	(3,1,'XINXIN','2019-03-19 03:31:03'),
	(4,1,'program','2019-03-25 06:50:56');

/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userOrganization
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userOrganization`;

CREATE TABLE `userOrganization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizationId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `organizationId` (`organizationId`,`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `userOrganization` WRITE;
/*!40000 ALTER TABLE `userOrganization` DISABLE KEYS */;

INSERT INTO `userOrganization` (`id`, `organizationId`, `userId`, `createTime`)
VALUES
	(1,1,1,'2019-02-23 15:38:45'),
	(2,1,2,'2019-02-23 15:38:45'),
	(3,1,3,'2019-02-23 15:38:45'),
	(4,1,4,'2019-02-23 15:38:45'),
	(5,1,5,'2019-02-23 15:38:45'),
	(6,1,6,'2019-02-23 15:38:45'),
	(7,1,7,'2019-02-23 15:38:45'),
	(8,1,8,'2019-02-23 15:38:45'),
	(9,1,9,'2019-02-23 15:38:45'),
	(10,1,10,'2019-02-23 15:38:45'),
	(11,1,11,'2019-02-23 15:38:45'),
	(12,1,12,'2019-02-23 15:38:45'),
	(13,1,13,'2019-02-23 15:38:45'),
	(14,1,14,'2019-02-23 15:38:45'),
	(16,1,16,'2019-02-23 15:38:45'),
	(17,1,17,'2019-02-23 15:38:45'),
	(18,1,18,'2019-02-23 15:38:45'),
	(19,1,19,'2019-02-23 15:38:45'),
	(20,1,20,'2019-02-23 15:38:45'),
	(21,1,21,'2019-02-23 15:38:45'),
	(22,2,21,'2019-02-23 15:38:45'),
	(23,1,23,'2019-02-23 15:38:45'),
	(24,1,24,'2019-02-23 15:38:45'),
	(25,1,25,'2019-02-23 15:38:45'),
	(26,1,26,'2019-02-23 15:38:45'),
	(27,1,27,'2019-02-23 15:38:45'),
	(28,1,28,'2019-02-23 15:38:45'),
	(29,1,29,'2019-02-23 15:38:45'),
	(30,1,30,'2019-02-23 15:38:45'),
	(31,1,31,'2019-02-23 15:38:45'),
	(32,1,32,'2019-02-23 15:38:45'),
	(33,1,33,'2019-02-23 15:38:45'),
	(34,1,34,'2019-02-23 15:38:45'),
	(35,1,35,'2019-02-23 15:38:45'),
	(36,1,36,'2019-02-23 15:38:45'),
	(37,1,37,'2019-02-23 15:38:45'),
	(38,1,38,'2019-02-23 15:38:45'),
	(39,1,39,'2019-02-23 15:38:45'),
	(40,1,40,'2019-02-23 15:38:45'),
	(41,1,41,'2019-02-23 15:38:45'),
	(42,1,42,'2019-02-23 15:38:45');

/*!40000 ALTER TABLE `userOrganization` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
