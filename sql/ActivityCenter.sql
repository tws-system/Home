# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.28)
# Database: ActivityCenter
# Generation Time: 2019-12-19 07:20:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `source` varchar(250) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `os` varchar(50) DEFAULT NULL,
  `eventType` varchar(50) DEFAULT NULL,
  `activityName` varchar(50) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;

INSERT INTO `event` (`id`, `userId`, `source`, `browser`, `os`, `eventType`, `activityName`, `remark`, `createTime`)
VALUES
	(1,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(2,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(3,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(4,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(5,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(6,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(7,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(8,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(9,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL),
	(10,NULL,'wechat','Chrome','Mac','click','none','关于我们',NULL);

/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table flyway_schema_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flyway_schema_history`;

CREATE TABLE `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;

INSERT INTO `flyway_schema_history` (`installed_rank`, `version`, `description`, `type`, `script`, `checksum`, `installed_by`, `installed_on`, `execution_time`, `success`)
VALUES
	(1,'1','create pv table','SQL','V1__create_pv_table.sql',283478600,'master','2019-04-03 09:37:07',124,1),
	(2,'2','create event table','SQL','V2__create_event_table.sql',-696805515,'master','2019-04-03 09:37:08',128,1),
	(3,'3','create user table','SQL','V3__create_user_table.sql',-188313205,'master','2019-04-17 02:04:21',166,1),
	(4,'4','Modify user and pv and event table change userId','SQL','V4__Modify_user_and_pv_and_event_table_change_userId.sql',267099697,'master','2019-04-22 06:26:48',387,1),
	(5,'5','add graduationTime column in user table','SQL','V5__add_graduationTime_column_in_user_table.sql',-2125631488,'master','2019-05-16 08:10:14',179,1);

/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pv`;

CREATE TABLE `pv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `source` varchar(250) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `os` varchar(50) DEFAULT NULL,
  `inch` varchar(50) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pv` WRITE;
/*!40000 ALTER TABLE `pv` DISABLE KEYS */;

INSERT INTO `pv` (`id`, `userId`, `source`, `browser`, `os`, `inch`, `ip`, `createTime`)
VALUES
	(634,NULL,'直接访问','Chrome','Mac','1440*900','10.205.20.60','2019-04-16 09:48:20'),
	(635,NULL,'直接访问','Chrome','Mac','1440*900','178.20.0.0','2019-04-16 01:48:27');

/*!40000 ALTER TABLE `pv` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `joinCity` varchar(250) DEFAULT NULL,
  `source` varchar(250) DEFAULT NULL,
  `recruitMessage` varchar(1000) DEFAULT NULL,
  `joinActivityTime` timestamp NULL DEFAULT NULL,
  `registerTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activityName` varchar(50) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `os` varchar(50) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `inch` varchar(50) DEFAULT NULL,
  `whenHear` varchar(100) DEFAULT NULL,
  `graduationTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `userId`, `joinCity`, `source`, `recruitMessage`, `joinActivityTime`, `registerTime`, `activityName`, `browser`, `os`, `platform`, `inch`, `whenHear`, `graduationTime`)
VALUES
	(15,2,'上海','直接访问','测试数据','2019-04-22 15:27:55','2019-04-22 15:02:28','测试数据','Chrome','Mac','pc','1440*900','一个月内',NULL),
	(16,3,'北京','直接访问','测试数据','2019-04-23 14:15:02','2019-04-22 15:21:17','测试数据','Chrome','Mac','pc','1440*900','半年内',NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
