# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.28)
# Database: DiffCenter
# Generation Time: 2019-12-19 07:22:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table assignment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assignment`;

CREATE TABLE `assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignmentId` int(11) DEFAULT NULL,
  `taskId` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `quizzesId` varchar(1000) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `operatorId` int(11) DEFAULT NULL,
  `operation` varchar(10) DEFAULT NULL,
  `operateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fromId` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `assignment` WRITE;
/*!40000 ALTER TABLE `assignment` DISABLE KEYS */;

INSERT INTO `assignment` (`id`, `assignmentId`, `taskId`, `title`, `quizzesId`, `type`, `visible`, `operatorId`, `operation`, `operateTime`, `fromId`)
VALUES
	(1,229,4,'如何提问','39','SUBJECTIVE_QUIZ',1,5,'0','2019-08-30 02:19:09',0),
	(2,230,5,'Markdown 的使用','40','SUBJECTIVE_QUIZ',1,5,'0','2019-08-30 02:19:09',0),
	(3,240,8,'编程题','15','HOMEWORK_QUIZ',1,5,'0','2019-08-30 02:19:09',0),
	(4,248,10,'简单客观题','248','BASIC_QUIZ',1,5,'0','2019-08-30 02:19:09',0),
	(5,276,2,'简单客观题','260,261','BASIC_QUIZ',1,31,'0','2019-08-30 02:19:09',0),
	(6,277,1,'编程题','10','HOMEWORK_QUIZ',1,5,'0','2019-08-30 02:19:09',0),
	(7,278,1,'主观题','53','SUBJECTIVE_QUIZ',1,5,'0','2019-08-30 02:19:09',0),
	(8,279,1,'简单客观题','263','BASIC_QUIZ',1,5,'0','2019-08-30 02:19:09',0),
	(9,280,16,'简单客观题','264','BASIC_QUIZ',1,31,'0','2019-08-30 02:19:09',0);

/*!40000 ALTER TABLE `assignment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table flyway_schema_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flyway_schema_history`;

CREATE TABLE `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;

INSERT INTO `flyway_schema_history` (`installed_rank`, `version`, `description`, `type`, `script`, `checksum`, `installed_by`, `installed_on`, `execution_time`, `success`)
VALUES
	(1,'1','create program table','SQL','V1__create_program_table.sql',-1734465781,'master','2019-08-27 01:59:41',24,1),
	(2,'2','create topic table','SQL','V2__create_topic_table.sql',-2094976088,'master','2019-08-27 01:59:41',25,1),
	(3,'3','create task table','SQL','V3__create_task_table.sql',1926939690,'master','2019-08-27 01:59:41',25,1),
	(4,'4','create assignment table','SQL','V4__create_assignment_table.sql',795532577,'master','2019-08-27 01:59:41',24,1),
	(5,'5','create history table','SQL','V5__create_history_table.sql',-1496054453,'master','2019-08-27 01:59:41',23,1),
	(6,'6','alert fromId in table','SQL','V6__alert_fromId_in_table.sql',-748697419,'master','2019-08-27 01:59:41',31,1);

/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `history`;

CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `jsonData` int(11) DEFAULT NULL,
  `operatorId` int(11) DEFAULT NULL,
  `operateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `program`;

CREATE TABLE `program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `programId` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `startTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `endTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `category` varchar(10) DEFAULT NULL,
  `introduction` text,
  `studentLink` int(1) DEFAULT NULL,
  `tutorLink` int(1) DEFAULT NULL,
  `isReadOnly` int(1) DEFAULT NULL,
  `fromId` int(11) DEFAULT NULL,
  `isExpired` int(1) DEFAULT NULL,
  `organizationId` int(11) DEFAULT NULL,
  `available` int(1) DEFAULT NULL,
  `operatorId` int(11) DEFAULT NULL,
  `operation` varchar(10) DEFAULT NULL,
  `operateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;

INSERT INTO `program` (`id`, `programId`, `title`, `startTime`, `endTime`, `category`, `introduction`, `studentLink`, `tutorLink`, `isReadOnly`, `fromId`, `isExpired`, `organizationId`, `available`, `operatorId`, `operation`, `operateTime`)
VALUES
	(122,1,'JavaScript pre courses','2017-12-22 00:00:00','2018-01-15 00:00:00','付费','\n#### 资源与工具\n\n- [如何使用 GitHub](https://www.zhihu.com/question/20070065)\n- [GitHub pages 搭建](https://pages.github.com/)\n- 廖雪峰的 Git 在线教程：<http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000>\n- Git 的基础内容的学习： <https://try.github.io/>\n\n\n\n#### 本节练习\n- 先注册一个GitHub账号，在GitHub中新建一个库，尝试`git clone`到本地进行修改，并`git push`上传到Github。\n- 编写一个静态页面，上传到自己的 GitHub Pages 中并回复链接地址。    \n\n',1,1,0,0,0,1,1,9,'0','2019-08-30 02:19:03'),
	(123,3,' dasfdasfa','2018-03-07 00:00:00','2018-01-05 00:00:00','免费','# 学习目标\n\n1. 了解ES6是什么\n2. 掌握学习内容中的ES6的新特性\n3. 根据推荐资料中的内容自学ES的其它特性（数组的拓展，Module）\n\n# 学习内容\n\n## 一、ECMAScript 6 简介\n\nECMAScript(ES6) 是JavaScript语言的下一代标准，已经在2015年6月正式发布了；在 JavaScript 的基础上做了重大的更新，提供了更优雅的语法和特性。\n\n#### ECMAScript 和 JavaScript 的关系\n\nECMAScript 和 JavaScript 的关系是，前者是后者的规格，后者是前者的一种实现（另外的ECMAScript 方言还有 Jscript 和 ActionScript ）。日常场合，这两个词是可以互换的。那为什么不叫 JavaScript 为换名字了呢？原因如下：\n\n1. 商标，1996年11月，JavaScript的创造者Netscape公司，决定将JavaScript提交给国际标准化组织ECMA，希望这种语言能够成为国际标准。根据授权协议，只有Netscape公司(Javascript 的创造者)可以合法地使用JavaScript这个名字，且 JavaScript 本身也已经被 Netscape 公司注册为商标\n2. 想体现这门语言的制定者是 ECMA，不是 Netscape，这样有利于保证这门语言的开放性和中立性。\n\n## 二、ES6 新特性\n###  1. let 和 const\n\nES6提供了两种新的声明变量的方式：let 和 const。\n#### 1）作用域\n\n let 和 const 相比于 var 声明的变量有块作用域，let 和 const 只在于当前的块作用域中有效。而 var 声明的变量是在函数作用域内有效。\n```\n{\n  let a = 1;  // a 只能在当前块中被访问\n  var b = 1;\n}\na // ReferenceError: a is not defined.\nb // 1\n```\nconst 和 let 的区别则在于， const 声明变量的同时必须立即给一个初始值，且无法在对该值进行修改。如果该初始值是一个对象，那么该对象的值是可以被修改的。\n\n#### 2）变量提升\n\n 变量如果在var 前使用，就会出现「变量提升」现象，值为undefined。let 声明的变量一定要在声明后使用，否则报错。\n```\n// var 的情况\nconsole.log(a); // 输出undefined\nvar a = 1;\n\n// let 的情况\nconsole.log(b); // 报错ReferenceError\nlet b = 1;\n```\n上述代码，`a`变量用`var`声明，会进行变量提升，脚本刚开始执行就已经存在变量`a`,但是变量`a`没有值，所以输出`undefined`；`b`变量用`let`声明，不会发生变量提升，在声明它之前，变量b是不存在的，这时如果用到它，就会抛出一个错误。\n#### 3）const 本质\n\nconst实际上保证的，并不是变量的值不得改动，而是变量指向的那个内存地址所保存的数据不得改动。\n```\nconst user = {};\n\n// 为 user 添加一个属性，可以成功\nuser.age = 23;\nuser.age // 23\n\n// 将 user 指向另一个对象，就会报错\nuser = {}; // TypeError: \"user\" is read-only\n```\n上面代码中，常量user储存的是一个地址，这个地址指向一个对象。不可变的只是这个地址，即不能把user指向另一个地址，但对象本身是可变的，所以依然可以为其添加新属性。\n\n### 2. 变量的解构赋值\n\nES6 允许按照一定模式，从数组和对象中提取值，对变量进行赋值，这被称为解构。\n```\n//以前变量赋值只能直接指定\nlet a = 1;\nlet b = 2;\nlet c = 3;\n//ES6 允许写成下面这样\nlet [a, b, c] = [1, 2, 3];\n```\n\n#### 1）数组的解构\n```\nvar [a,b,c] = [1,2,3] // 结果 a=1 b=2 c=3\nvar [a,[b,c]] = [1,[2,3]] // 结果 a=1 b=2 c=3\n\n//解构不成功\nvar [a] = [];  // a = undefined   \nvar [a, b] = [1]; //a=1 b=undefined\n\n//不完全解构\nvar [a] = [1,2]; //a=1\n\n//默认值\nvar [a=1] = [] //a=1\n```\n**如果等号的右边不是数组（或者严格地说，不是可遍历的结构），那么将会报错**\n```javascript\nlet [a] = 1;\nlet [a] = false;\nlet [a] = NaN;\nlet [a] = undefined;\nlet [a] = null;\nlet [a] = {};\n```\n#### 2）对象的解构\n```\nvar { b, a } = { a: \"aaa\", b: \"bbb\" };\na // \"aaa\"\nb // \"bbb\"\n\nvar { c } = { a: \"aaa\", b: \"bbb\" };\nc // undefined\n\n//变量名与属性名不一致，必须写成下面这样\nlet { a: hello } = { a: \'aaa\', hello: \'bbb\' };\nhello // \"aaa\"\n\n//对象的解构赋值的内部机制，是先找到同名属性，然后再赋给对应的变量\nlet { a: hello } = { a: \"aaa\", hello: \"bbb\" };\nhello // \"aaa\"\na // error: foo is not defined\n\n//对象的解构也可以指定默认值\nvar {x = 3} = {};\nx // 3\nvar {x, y = 5} = {x: 1};\nx // 1\ny // 5\n```\n#### 3）字符串的解构赋值\n\n字符串也可以解构赋值。这是因为此时，字符串被转换成了一个类似数组的对象。\n```\nconst [a, b, c, d, e] = \'hello\';\na // \"h\"\nb // \"e\"\nc // \"l\"\nd // \"l\"\ne // \"o\"\n\n//类似数组的对象都有一个length属性，因此还可以对这个属性解构赋值\nlet {length : len} = \'hello\';\nlen // 5\n```\n### 3.字符串拓展\n#### 1）模版字符串\n\n模板字符串（template string）是增强版的字符串，用反引号（`）标识。它可以当作普通字符串使用，也可以用来定义多行字符串（类似 pre 标签的作用），或者在字符串中嵌入变量。\n```\n// 普通字符串 想要换行需要加上 \'\\n\'\n`In JavaScript \'\\n\' is a line-feed.`\n\n// 多行字符串\n`In JavaScript this is\n not legal.`\n\n// 字符串中嵌入变量\nvar name = \"ac\", time = \"today\";\n`Hello ${name}, how are you ${time}?` // Hello ac, how are you today? \n```\n#### 2）字符串的遍历器接口\n\nES6 为字符串添加了遍历器接口，使得字符串可以被for...of循环遍历\n```\nfor (let codePoint of \'hello\') {\n  console.log(codePoint)\n}\n// \"h\"\n// \"e\"\n// \"l\"\n// \"l\"\n// \"o\"\n```\n#### 3）includes(), startsWith(), endsWith() \n\n在这之前，JavaScript 只有indexOf方法，可以用来确定一个字符串是否包含在另一个字符串中。\n\n**ES6 又提供了三种新方法：**\n- includes()：返回布尔值，表示是否找到了参数字符串。\n- startsWith()：返回布尔值，表示参数字符串是否在原字符串的头部。\n- endsWith()：返回布尔值，表示参数字符串是否在原字符串的尾部。\n```\nlet s = \'Hello world!\';\ns.startsWith(\'Hello\') // true\ns.endsWith(\'!\') // true\ns.includes(\'o\') // true\n这三个方法都支持第二个参数，表示开始搜索的位置。\n\nlet s = \'Hello world!\';\ns.startsWith(\'world\', 6) // true\ns.endsWith(\'Hello\', 5) // true\ns.includes(\'Hello\', 6) // false\n```\n上面代码表示，使用第二个参数n时，endsWith的行为与其他两个方法有所不同。它针对前n个字符，而其他两个方法针对从第n个位置直到字符串结束。\n#### 4）repeat()\n\n`repeat`方法返回一个新字符串，表示将原字符串重复n次。\n```\n\'x\'.repeat(3) // \"xxx\"\n\'hello\'.repeat(2) // \"hellohello\"\n\'na\'.repeat(0) // \"\"\n\n//参数如果是小数，会被取整\n\'na\'.repeat(2.9) // \"nana\"\n\n//如果repeat的参数是负数或者Infinity，会报错。\n\'na\'.repeat(Infinity)\n// RangeError\n\'na\'.repeat(-1)\n// RangeError\n\n//参数是 0 到-1 之间的小数，则等同于 0\n\'na\'.repeat(-0.9) // \"\"\n\n//参数NaN等同于 0。\n\'na\'.repeat(NaN) // \"\"\n\n//如果repeat的参数是字符串，则会先转换成数字。\n\'na\'.repeat(\'na\') // \"\"\n\'na\'.repeat(\'3\') // \"nanana\"\n```\n#### 5）padStart()，padEnd()\n\nES2017 引入了字符串补全长度的功能。如果某个字符串不够指定长度，会在头部或尾部补全。padStart()用于头部补全，padEnd()用于尾部补全。\n```\n\'x\'.padStart(5, \'ab\') // \'ababx\'\n\'x\'.padStart(4, \'ab\') // \'abax\'\n\n\'x\'.padEnd(5, \'ab\') // \'xabab\'\n\'x\'.padEnd(4, \'ab\') // \'xaba\'\n\n//如果原字符串的长度，等于或大于最大长度，则字符串补全不生效，返回原字符串\n\'xxx\'.padStart(2, \'ab\') // \'xxx\'\n\'xxx\'.padEnd(2, \'ab\') // \'xxx\'\n\n//如果省略第二个参数，默认使用空格补全长度\n\'x\'.padStart(4) // \'   x\'\n\'x\'.padEnd(4) // \'x   \'\n```\n### 4.数值的扩展\n#### 1）Number.isFinite(), Number.isNaN()\nES6 在Number对象上，新提供了Number.isFinite()和Number.isNaN()两个方法。\n\nNumber.isFinite()用来检查一个数值是否为有限的（finite），即不是Infinity。\n```\nNumber.isFinite(15); // true\nNumber.isFinite(0.8); // true\nNumber.isFinite(NaN); // false\nNumber.isFinite(Infinity); // false\nNumber.isFinite(-Infinity); // false\nNumber.isFinite(\'foo\'); // false\nNumber.isFinite(\'15\'); // false\nNumber.isFinite(true); // false\n```\nNumber.isNaN()用来检查一个值是否为NaN\n```\nNumber.isNaN(NaN) // true\nNumber.isNaN(15) // false\nNumber.isNaN(\'15\') // false\nNumber.isNaN(true) // false\nNumber.isNaN(9/NaN) // true\nNumber.isNaN(\'true\' / 0) // true\nNumber.isNaN(\'true\' / \'true\') // true\n```\n它们与传统的全局方法`isFinite()`和`isNaN()`的区别在于，传统方法先调用`Number()`将非数值的值转为数值，再进行判断，而这两个新方法只对数值有效，`Number.isFinite()`对于非数值一律返回`false`, `Number.isNaN()`只有对于`NaN`才返回`true`，非`NaN`一律返回`false`。\n\n#### 2）Number.isInteger()\n\nNumber.isInteger()用来判断一个数值是否为整数\n```\nNumber.isInteger(5) // true\nNumber.isInteger(5.1) // false\n\n//JavaScript 内部，整数和浮点数采用的是同样的储存方法，所以 5 和 5.0 被视为同一个值\nNumber.isInteger(5) // true\nNumber.isInteger(5.0) // true\n```\n### 5. Math 对象的扩展\n#### 1）Math.trunc() \n`Math.trunc`方法用于去除一个数的小数部分，返回整数部分\n```\nMath.trunc(4.1) // 4\nMath.trunc(4.9) // 4\nMath.trunc(-4.1) // -4\nMath.trunc(-4.9) // -4\nMath.trunc(-0.1234) // -0\n```\n\n\n#### 2）Math.sign() \n\n`Math.sign`方法用来判断一个数到底是正数、负数、还是零。对于非数值，会先将其转换为数值。\n\n它会返回五种值：\n- 参数为正数，返回+1；\n- 参数为负数，返回-1；\n- 参数为 0，返回0；\n- 参数为-0，返回-0;\n- 其他值，返回NaN。\n```\nMath.sign(-5) // -1\nMath.sign(5) // +1\nMath.sign(0) // +0\nMath.sign(-0) // -0\nMath.sign(NaN) // NaN\n```\n### 6.函数的扩展\n#### 1）函数参数的默认值\n\nES6 允许为函数的参数设置默认值，即直接写在参数定义的后面\n```\nfunction log(x, y = \'World\') {\n  console.log(x, y);\n}\n\nlog(\'Hello\') // Hello World\nlog(\'Hello\', \'China\') // Hello China\nlog(\'Hello\', \'\') // Hello\n```\n#### 2）rest 展开运算符\nES6 引入 rest 参数（形式为...变量名），用于获取函数的多余参数，这样就不需要使用arguments对象了。rest 参数搭配的变量是一个数组，该变量将多余的参数放入数组中。\n```\nfunction example(...values){\n    console.log(values)// console: [1,2,3,4]\n}\n\nexample(1,2,3,4) \nvar a = [1,2,3]\nvar b = [...a,4,5,6] //b = [1,2,3,4,5,6]\n```\n#### 3） 箭头函数\n\nES6 提供了新的方式 `=>` 来定义函数\n```\nvar func = parm => parm\n等同于\nvar func = function (parm){\n    return parm\n}\n```\n\n如果函数没有参数或有多个参数，那么：\n```\nvar func = () => //some code\n等同于\nvar func = function (){\n	some code\n}\n\nvar func = (parm1,parm2) => //some code\n等同于\nvar func = function (parm1,parm2){\n	some code\n}\n```\n\n如果箭头函数的函数体只包含一行代码，则可以不需要写大括号以及 return 语句返回（如果有返回值）\n```\nvar sum = (num1,num2) => num1+num2\n等同于\nvar sum = (num1,num2) => {return num1+num2}\n等同于\nvar sum = function(num1,num2){return num1+num2}\n```\n\n箭头函数使得表达更加简洁\n```\n[1,2,3].map( item=> 2 * item)\n等同于\n[1,2,3].map(function(item){\n    return item * 2\n})\n\n[1,3,2].sort((a,b) => a - b)\n```\n### 7.数组的扩展\n\n#### 1）扩展运算符\n\n扩展运算符（spread）是三个点（...）。它好比 rest 参数的逆运算，将一个数组转为用逗号分隔的参数序列。\n```\nconsole.log(...[1, 2, 3])\n// 1 2 3\n\nconsole.log(1, ...[2, 3, 4], 5)\n// 1 2 3 4 5\n```\n#### 2）扩展运算符的应用\n\n- **复制数组**\n\n数组是复合的数据类型，直接复制的话，只是复制了指向底层数据结构的指针，而不是克隆一个全新的数组。\n```\nconst a = [1, 2];\nconst b = a;\n\nb[0] = 2;\na // [2, 2]\n```\n上面代码中，a2并不是a1的克隆，而是指向同一份数据的另一个指针。修改a2，会直接导致a1的变化。\n\nES5 只能用变通方法来复制数组\n```\nconst a = [1, 2];\nconst b = a.concat();\n\nb[0] = 2;\na // [1, 2]\n```\n\n扩展运算符提供了复制数组的简便写法\n```\nconst a1 = [1, 2];\n// 写法一\nconst a2 = [...a1];\n// 写法二\nconst [...a2] = a1;\n```\n- **合并数组**\n\n扩展运算符提供了数组合并的新写法\n```\nconst arr1 = [\'a\', \'b\'];\nconst arr2 = [\'c\'];\nconst arr3 = [\'d\', \'e\'];\n\n// ES5 的合并数组\narr1.concat(arr2, arr3);\n// [ \'a\', \'b\', \'c\', \'d\', \'e\' ]\n\n// ES6 的合并数组\n[...arr1, ...arr2, ...arr3]\n// [ \'a\', \'b\', \'c\', \'d\', \'e\' ]\n```\n- **字符串**\n\n扩展运算符还可以将字符串转为真正的数组\n```\n[...\'hello\']\n// [ \"h\", \"e\", \"l\", \"l\", \"o\" ]\n```\n#### 3） Array.from() \n\nArray.from方法用于将两类对象转为真正的数组：类似数组的对象（array-like object）和可遍历（iterable）的对象（包括 ES6 新增的数据结构 Set 和 Map）。\n```\nlet arrayLike = {\n    \'0\': \'a\',\n    \'1\': \'b\',\n    \'2\': \'c\',\n    length: 3\n};\n\n// ES5的写法\nvar arr1 = [].slice.call(arrayLike); // [\'a\', \'b\', \'c\']\n\n// ES6的写法\nlet arr2 = Array.from(arrayLike); // [\'a\', \'b\', \'c\']\n\nArray.from(\'hello\')\n// [\'h\', \'e\', \'l\', \'l\', \'o\']\n\nlet namesSet = new Set([\'a\', \'b\'])\nArray.from(namesSet) // [\'a\', \'b\']\n```\n#### 4）Array.of() \n\nArray.of方法用于将一组值，转换为数组\n```\nArray.of(3, 11, 8) // [3,11,8]\nArray.of(3) // [3]\nArray.of(3).length // 1\n```\n#### 5）数组实例的 find() 和 findIndex()\n\n数组实例的`find`方法，用于找出第一个符合条件的数组成员。它的参数是一个回调函数，所有数组成员依次执行该回调函数，直到找出第一个返回值为`true`的成员，然后返回该成员。如果没有符合条件的成员，则返回`undefined`。\n```\n[1, 4, -5, 10].find((n) => n < 0)\n// -5\n\n//find方法的回调函数可以接受三个参数，依次为当前的值、当前的位置和原数组\n[1, 5, 10, 15].find(function(value, index, arr) {\n  return value > 9;\n}) // 10\n```\n\n数组实例的`findIndex`方法的用法与`find`方法非常类似，返回第一个符合条件的数组成员的位置，如果所有成员都不符合条件，则返回`-1`\n```\n[1, 5, 10, 15].findIndex(function(value, index, arr) {\n  return value > 9;\n}) // 2\n```\n#### 6）数组实例的 includes()\n\nArray.prototype.includes方法返回一个布尔值，表示某个数组是否包含给定的值，与字符串的includes方法类似。ES2016 引入了该方法。\n```\n[1, 2, 3].includes(2)     // true\n[1, 2, 3].includes(4)     // false\n[1, 2, NaN].includes(NaN) // true\n```\n该方法的第二个参数表示搜索的起始位置，默认为0\n```\n[1, 2, 3].includes(3, 3);  // false\n[1, 2, 3].includes(3, -1); // true\n```\n## 推荐资料\n- [ECMAScript 6 入门](http://es6.ruanyifeng.com/#docs/destructuring)\n\n# 学习目标\n\n1. 了解ES6是什么\n2. 掌握学习内容中的ES6的新特性\n3. 根据推荐资料中的内容自学ES的其它特性（数组的拓展，Module）\n\n# 学习内容\n\n## 一、ECMAScript 6 简介\n\nECMAScript(ES6) 是JavaScript语言的下一代标准，已经在2015年6月正式发布了；在 JavaScript 的基础上做了重大的更新，提供了更优雅的语法和特性。\n\n#### ECMAScript 和 JavaScript 的关系\n\nECMAScript 和 JavaScript 的关系是，前者是后者',1,1,0,0,0,2,1,9,'0','2019-08-30 02:19:03'),
	(124,4,' 实验室纳新题','2018-03-01 00:00:00','2018-03-10 00:00:00','付费','## 实验室纳新\n',1,1,0,0,0,1,1,31,'0','2019-08-30 02:19:03'),
	(125,5,' test','2018-01-26 00:00:00','2018-01-11 00:00:00','付费',' test',1,1,0,0,0,1,1,5,'0','2019-08-30 02:19:03');

/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table task
# ------------------------------------------------------------

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) DEFAULT NULL,
  `topicId` int(11) DEFAULT NULL,
  `fromId` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` text,
  `visible` int(1) DEFAULT NULL,
  `operatorId` int(11) DEFAULT NULL,
  `operation` varchar(10) DEFAULT NULL,
  `operateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;

INSERT INTO `task` (`id`, `taskId`, `topicId`, `fromId`, `title`, `content`, `visible`, `operatorId`, `operation`, `operateTime`)
VALUES
	(263,1,1,0,'1 教学方法','# 1 教学方法',1,0,'0','2019-08-30 02:19:06'),
	(264,2,1,0,'2 软件和工具的安装','## 软件和工具的安装',1,0,'0','2019-08-30 02:19:06'),
	(265,4,2,0,'如何提问','qwerqw',1,0,'0','2019-08-30 02:19:06'),
	(266,5,2,0,'Markdown 使用','# Markdown 使用',1,0,'0','2019-08-30 02:19:06');

/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table topic
# ------------------------------------------------------------

DROP TABLE IF EXISTS `topic`;

CREATE TABLE `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topicId` int(11) DEFAULT NULL,
  `programId` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `fromId` int(11) DEFAULT NULL,
  `operatorId` int(11) DEFAULT NULL,
  `operation` varchar(10) DEFAULT NULL,
  `operateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;

INSERT INTO `topic` (`id`, `topicId`, `programId`, `title`, `visible`, `fromId`, `operatorId`, `operation`, `operateTime`)
VALUES
	(122,1,1,'Start',1,0,0,'0','2019-08-30 02:19:04'),
	(123,2,1,'基本技能的',1,0,0,'0','2019-08-30 02:19:04'),
	(124,3,1,'高级练习',1,0,0,'0','2019-08-30 02:19:04'),
	(125,5,4,'基础锻炼',1,0,0,'0','2019-08-30 02:19:04');

/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
